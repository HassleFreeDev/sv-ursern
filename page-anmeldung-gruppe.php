<?php get_header(); ?>

<div class="container container-page">
  <header class="page-head">
    <h1><?php the_title(); ?></h1>
  </header>

  <?php if ( have_posts() ) : 
          while ( have_posts() ) : 
            the_post(); ?>
    
            <?php the_content(); ?>

      <?php endwhile; else : ?>
  <?php endif; ?>

</div>



<?php get_footer(); ?>