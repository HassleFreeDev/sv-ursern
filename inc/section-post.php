<!-- Get the content from the posts // from the backend -->
<?php if ( have_posts() ) : ?>
	<?php while ( have_posts() ) : the_post(); ?>    

  <div class="card">
      <h3 class="heading-post">
        <!-- Display the title -->
       <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a> 
      </h3>

      <?php if ( has_post_thumbnail() ) : ?>
        <a class="thumbnail" href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
            <?php the_post_thumbnail(); ?>
        </a>
      <?php endif; ?>

      <p>
        <!-- Show max. 55 characters/ words -->
          <?php the_excerpt(); ?>
      </p>
      
      <!-- Clear float -->
      <div class="clear"></div>

      <small>
        <!-- Display the post date -->
        <span class="date">
          <?php echo get_the_date();?>
        </span>
      </small>
        
    </div>

  <?php endwhile;
    /* Pagination, with two shown pages between next and prev */
    the_posts_pagination( array( 'mid_size' => 2 ) ); 

    else: ?>

      <div class="container error">
        <div class="error-msg">
            
          <p>Zurzeit sind keine Beiträge verfügbar</p>
          
        </div>
      </div>
    
<?php endif; ?>
