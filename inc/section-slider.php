<!-- Code for slickslider and ACF -->
<!-- Includes code for the lightbox -->
<?php 
    $images = get_field('slider');
    $size = 'full';
    if( $images ): ?>
    <!-- lightbox -->
      <div class="portfolio-slider">
        
        <!-- slider -->
        <div class="variable-width">
          <?php foreach( $images as $image): ?>
            
            <a href="<?php echo esc_url($image['url']); ?>" target="_blank" rel="noreferrer">
              <img src="<?php echo esc_url($image['sizes']['medium']); ?>" alt="">
            </a>
            
            <?php endforeach; ?>
          </div>
          
        </div>

  <?php endif; ?>