<?php

/* Enqueue stylesheets */
  function load_stylesheets() {
    wp_register_style('slick', get_template_directory_uri() . '/slick/slick.css', array(), '', 'all');
    wp_enqueue_style('slick');
    
    wp_register_style('slick_theme', get_template_directory_uri() . '/slick/slick-theme.css', array(), '', 'all');
    wp_enqueue_style('slick_theme');
    
    wp_register_style('slick_lightbox', get_template_directory_uri() . '/slick-lightbox-master/dist/slick-lightbox.css', array(), '', 'all');
    wp_enqueue_style('slick_lightbox');
    
    wp_register_style('main', get_template_directory_uri() . '/dist/css/main.css', array(), '', 'all');
    wp_enqueue_style('main');
    /* wp_register_style('font-awesome', get_template_directory_uri() . '/node_modules/@fortawesome/fontawesome-free/css/all.css', array(), '', 'all');
    wp_enqueue_style('font-awesome'); */

  }
  add_action('wp_enqueue_scripts', 'load_stylesheets');

/* Enqueue stylesheets */
  function load_scripts() {
    wp_enqueue_script('jquery');

    wp_register_script('slick', get_template_directory_uri() . '/slick/slick.min.js', array(), '', true);
    wp_enqueue_script('slick');

    wp_register_script('slick_init', get_template_directory_uri() . '/dist/js/slick_init.js', array(), '', true);
    wp_enqueue_script('slick_init');

    wp_register_script('slick_lightbox', get_template_directory_uri() . '/slick-lightbox-master/dist/slick-lightbox.min.js', array(), '', true);
    wp_enqueue_script('slick_lightbox');
      
    wp_register_script('app', get_template_directory_uri() . '/dist/js/app.js', array(), '', true);
    wp_enqueue_script('app');
  }
  add_action('wp_enqueue_scripts', 'load_scripts');

/* Add cutsom menu to backend */
function register_custom_menus() {
  register_nav_menus(
    array(
      'header-menu'     => __( 'Header Menu' ),
      'footer-menu'     => __( 'Footer Menu' )
     )
   );
}
add_action( 'init', 'register_custom_menus' );

/* Add custom header to customizer */
function schuetzenverein_cstm_header() {
  $args = array(
      'default-image'      => get_template_directory_uri() . 'img/SVUH_logo.png',
      'default-text-color' => 'fff',
      'width'              => 50,
      'height'             => 50,
      'flex-width'         => true,
      'flex-height'        => true,
  );
  /* Add theme support (make it avaiable in the customizer) */
  add_theme_support( 'custom-header', $args);
}
add_action( 'after_setup_theme', 'schuetzenverein_cstm_header' );

/* Add thumbnail support for posts  */
add_theme_support('post-thumbnails');

the_post_thumbnail( 'thumbnail' ); // Thumbnail (default 150px x 150px max)
the_post_thumbnail( 'medium' ); // Medium resolution (default 300px x 300px max)
the_post_thumbnail( 'medium_large' ); // Medium-large resolution (default 768px x no height limit max)
the_post_thumbnail( 'large' ); // Large resolution (default 1024px x 1024px max)
the_post_thumbnail( 'full' ); // Original image resolution (unmodified)

/* Exclude pages from the search.php if is not admin (not in the backend) */
if(!is_admin()) {
  function wpb_search_filter($query) {
    if ($query->is_search) {
    $query->set('post_type', 'post');

    return $query;
    }
  }

add_filter('pre_get_posts','wpb_search_filter');
}

/* Filter the excerpt length to 'n' words */
function wpdocs_custom_excerpt_length( $length ) {
  return  30;
}
add_filter( 'excerpt_length', 'wpdocs_custom_excerpt_length', 999 );


/* Link the <Read More> (usage with <?php the_excerpt(); ?>) to the Post */
function wpdocs_excerpt_more($more) {
  if ( !is_single() ) {
      $more = sprintf( '<div class="read-more-wrap"> <a class="read-more" href="%1$s">%2$s</a> </div>',
          get_permalink( get_the_ID() ),
          __( 'Lies mehr', 'textdomain' )
      );
  }

  return $more;
}
add_filter( 'excerpt_more', 'wpdocs_excerpt_more' );