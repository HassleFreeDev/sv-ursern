<?php get_header(); ?>

<div class="container container-page">
  <header class="page-head">
    <h1><?php the_title(); ?></h1>
  </header>

  <?php if( have_rows('praesident') ): ?>
      <?php while( have_rows('praesident') ): the_row(); 
  
          // Get sub field values.
          $position = get_sub_field('position');
          $name = get_sub_field('name');
          ?>

          <div id="hero">
              <div class="content">
                <div class="position">
                  <?php the_sub_field('position'); ?>
                </div>
                <div class="name">
                  <?php the_sub_field('name'); ?>
                </div>
              </div>
          </div>

  <?php endwhile; ?>

  <?php endif; ?>

  <?php if ( have_posts() ) : 
          while ( have_posts() ) : 
            the_post(); ?>
    
            <?php the_content(); ?>

      <?php endwhile; else : ?>
  <?php endif; ?>

  <p>Oder senden Sie eine Email an: <span class="email-field-acf"><?php the_field('email'); ?></span></p>

  <div class="anfahrt">
    <?php the_field('wo'); ?>
  </div>

  <div class="anfahrtsplan">
    <?php 
      $image = get_field('anfahrtsplan');
      $size = 'full'; // (thumbnail, medium, large, full or custom size)
      if( $image ) {
          echo wp_get_attachment_image( $image, $size );
      } ?>
  </div>

  <?php get_template_part('inc/section', 'slider'); ?> 

</div>



<?php get_footer(); ?>