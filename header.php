<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">

  <!-- Display site name, set in the backend, in the tab bar -->
  <title><?php bloginfo('name'); ?></title>

  <!-- Insert all needed head tags - !important -->
  <?php wp_head(); ?>

</head>
<body <?php body_class('my-class'); ?>>

<header class="custom-header">
  <div class="container">

    <div class="header-container">
      <div class="flex-left">
        <img class="logo" alt="Schützenverein Logo" src="<?php header_image(); ?>" width="<?php echo absint( get_custom_header()->width ); ?>" height="<?php echo absint( get_custom_header()->height ); ?>">

        <a href="<?php echo get_option("siteurl"); ?>">
          <h1>Schützenverein Ursern Hospental</h1>
        </a>

      </div>

      <div class="flex-right">
        <div class="burger"></div>
      </div>
    </div>
  </div>

  <div class="hidden-nav">

    <div class="navigation-heading">
      <div id="close-btn">
        <div class="x-left"></div>
        <div class="x-right"></div>
      </div>
      <h4>Navigation</h4>
    </div>

    <?php get_search_form(); ?>

    <div class="navigation-page-heading">
        <h3>Seiten</h3>

        <?php wp_nav_menu( array( 'theme_location' => 'header-menu' ) ); ?>
    </div>
    
  </div>
</header>
<!-- Pushes main content 100px down, because of fixe nav bar -->
<div class="top-100"></div>
