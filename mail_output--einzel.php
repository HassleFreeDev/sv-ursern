<!-- .php, because the auto formatter will destroy the layout for backend contact form 7 -->
Von: [your-name] <[email]>

Anmeldung Einzelschütze(n)

Kontakt:
[your-name]
[adresse]
[plz-ort]
[email]
[handy]
[festnetz]

Kommentar:
[your-message-kontakt]

---------------------------------------

Vereinsname: [vereinsname]
Vereinssitz: [vereinssitz]
Vereinskategorie: [vereinskategorie]

---------------------------------------

Einzelschütze 1:
Name: [einzelschuetze1]
Jahrgang: [jahrgang1]
Lizensnummer: [lizensnummer1]
Gewehr: [gewehr1]

---------------------------------------

Einzelschütze 2:
Name: [einzelschuetze2]
Jahrgang: [jahrgang2]
Lizensnummer: [lizensnummer2]
Gewehr: [gewehr2]

---------------------------------------

Einzelschütze 3:
Name: [einzelschuetze3]
Jahrgang: [jahrgang3]
Lizensnummer: [lizensnummer3]
Gewehr: [gewehr3]

---------------------------------------

Einzelschütze 4:
Name: [einzelschuetze4]
Jahrgang: [jahrgang4]
Lizensnummer: [lizensnummer4]
Gewehr: [gewehr4]

---------------------------------------

Schiesszeiten: [schiesszeiten]

Schiesszeit: [schiesszeit]

Kommentar: [schiesszeit-kommentar]

---------------------------------------

IBAN/Kontonummer: [IBAN]

Anschrift Bank: [anschrift-bank]


-- 
Diese E-Mail wurde über ein Kontaktformular auf SV-Ursern (https://sv-ursern.de) verschickt.