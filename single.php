<!-- Include header.php -->
<?php get_header(); ?>

<div class="container">
   
   <?php if ( have_posts() ) : ?>
	  <?php while ( have_posts() ) : the_post(); ?>    

  <div class="card my-1">
      <h3 class="heading-post">
        <!-- Display the title -->
        <?php the_title(); ?>
      </h3>

      <?php if ( has_post_thumbnail() ) : ?>
        <a class="thumbnail" href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
            <?php the_post_thumbnail(); ?>
        </a>
      <?php endif; ?>

      <?php the_content(); ?>

      <div class="clear"></div>

      <small>
        <!-- Display the post date -->
        <span class="date">
          <?php echo get_the_date();?>
        </span>

        <!-- Display the categories -->
        <?php echo get_the_category_list(', '); ?>
      </small>
    </div>

    <?php endwhile; else: endif; ?>

    <?php

    /* acf repeater field - PDF files */
    // Check rows exists.
    if( have_rows('pdf') ):

        // Loop through rows.
        while( have_rows('pdf') ) : the_row();

            // Load sub field value.
            $dateiName = get_sub_field('datei_name');
            $pdfSub = get_sub_field('pdf_sub'); ?>
            
            <div class="pdf-wrap">
              <p><?php echo $dateiName; ?> <a href="<?php echo $pdfSub['url']; ?>" target="_blank" rel="noopener noreferrer"><?php echo $pdfSub['filename']; ?></a></p>
            </div>
            

       <?php // End loop.
        endwhile;

    // No value.
    else :
        // Do something...
    endif; ?>

    <?php get_template_part('inc/section', 'slider'); ?> 
  
   </div>

</div>

<!-- Include footer.php -->
<?php get_footer(); ?>