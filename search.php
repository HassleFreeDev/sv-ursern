<?php get_header(); ?>

<div class="container">
  <div class="search-result-number my-1">
    <!-- Put search result output //number// in a variable -->
    <?php $count = $wp_query->found_posts; ?>
  
    <!-- Output variable -->
    <div class="search-count">
      <?php echo $count; ?> 
    </div>
  
    <div class="search-output">
      <!-- Condition if output is 1 -->
      <?php if($count == 1) {
        _e( 'Suchergebnis für: ', 'locale' ); ?>
        <div class="search-input">
          <?php the_search_query();?>
        </div>
      <!-- or higher / lower than 1  -->
      <?php } else {
        _e( 'Suchergebnisse für: ', 'locale' ); ?>
        <div class="search-input">
          <?php the_search_query(); ?>
        </div>
      <?php } ?>
    </div>
  </div>
</div>

<div class="container">
  <?php get_template_part('inc/section', 'post'); ?> 
</div>

<?php get_footer(); ?>