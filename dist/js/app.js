const burger = document.querySelector('.flex-right');
const hiddenNav = document.querySelector('.hidden-nav');
const closeBtn = document.getElementById('close-btn');


burger.addEventListener('click', () => {
  hiddenNav.classList.add('navbar-open');
});

closeBtn.addEventListener('click', () => {
  hiddenNav.classList.remove('navbar-open');
});

