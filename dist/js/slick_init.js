/* Using jQuery in the conflict free mode - write jQuery Code between the curly braces */
jQuery(document).ready(function ($) {
  $('.variable-width').slick({
    dots: true,
    infinite: true,
    speed: 300,
    slidesToShow: 1,
    centerMode: true,
    variableWidth: true,
    arrows: true
  });
});

/* Lightbox */

jQuery(document).ready(function ($) {

  $('.portfolio-slider').slickLightbox({

    itemSelector: 'a',
    navigateByKeyboard: true
  });
});