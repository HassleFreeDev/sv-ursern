<!-- .php, because the auto formatter will destroy the layout for backend contact form 7 -->
Von: [your-name] <[email]>

Anmeldung Gruppe

Kontakt:
[your-name]
[adresse]
[plz-ort]
[email]
[handy]
[festnetz]

Kommentar:
[your-message-kontakt]

---------------------------------------

Vereinsname: [vereinsname]
Vereinssitz: [vereinssitz]
Vereinskategorie: [vereinskategorie]

---------------------------------------
Gruppenname: [gruppenname]

Schütze 1:
Name: [schuetze1]
Jahrgang: [jahrgang1]
Lizensnummer: [lizensnummer1]
Gewehr: [gewehr1]

---------------------------------------

Schütze 2:
Name: [schuetze2]
Jahrgang: [jahrgang2]
Lizensnummer: [lizensnummer2]
Gewehr: [gewehr2]

---------------------------------------

Schütze 3:
Name: [schuetze3]
Jahrgang: [jahrgang3]
Lizensnummer: [lizensnummer3]
Gewehr: [gewehr3]

---------------------------------------

Schütze 4:
Name: [schuetze4]
Jahrgang: [jahrgang4]
Lizensnummer: [lizensnummer4]
Gewehr: [gewehr4]

---------------------------------------


Schiesszeiten: [schiesszeiten]

Schiesszeit: [schiesszeit]

Kommentar: [schiesszeit-kommentar]

---------------------------------------


IBAN/Kontonummer: [IBAN]

Anschrift Bank: [anschrift-bank]


-- 
Diese E-Mail wurde über ein Kontaktformular auf SV-Ursern (https://sv-ursern.de) verschickt.